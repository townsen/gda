require 'mkmf'

# :stopdoc:

# required for brew installs of libgda as libxml2 is not included by default
if RUBY_PLATFORM =~ /darwin/
  ENV['PKG_CONFIG_PATH'] = '/usr/local/opt/libxml2/lib/pkgconfig:/usr/local/lib/pkgconfig'
end

dir_config 'libgda'

def asplode missing
  msg = "#{missing} is missing."
  msg += " Try 'brew install #{missing}'" if RUBY_PLATFORM =~ /darwin/
  abort msg
end

pkg_config 'libgda-5.0'
find_header('libgda/sql-parser/gda-sql-parser.h') || asplode("libgda")

create_makefile 'gda/gda'

# :startdoc:
